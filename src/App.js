import "./App.css";

import { useState } from "react";

function App() {
  const [loading, setLoading] = useState(false);
  const [complete, setComplete] = useState(false);

  const handleOnClick = () => {
    setLoading(true);

    setTimeout(() => {
      setLoading(false);
      setComplete(true);
    }, 3000);
  };

  return (
    <div className="App">
      <button
        className={`button is-large is-outlined ${
          loading ? "is-loading" : ""
        } ${complete ? "is-success" : ""}`}
        onClick={handleOnClick}
        disabled={loading || complete}
      >
        {complete && <i className="fas fa-check" />}
        {!complete ? "Mark Complete" : "Complete"}
      </button>
    </div>
  );
}

export default App;
